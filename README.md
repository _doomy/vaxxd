# vaxxd

this uses an API endpoint hosted by Albertson's pharmacy to check for covid vaccine stock within a US zip code.

## Running

1. clone this repo and `cd` into it

2. `cargo run -- ZIPCODE` where ZIPCODE is replaced by your zip. e.g., `cargo run -- 90210`

3. let it run, and it will check for changes every 10 seconds. if a new site is found, you will get a desktop notification. 

4. when you get a notification, navigate back to the console and you should see a prompt: `✅ New listing: Albertsons...` with a clickable link. you can get an appointment through the link provided.

## Range

1. you can provide a range (in kilometers, which is kind of dumb for people with us zip codes but im too lazy to do a km to mi conversion fn) like this:

```
cargo run -- 90210 -d 15
```

which would only show results in a 15km radius from that zip. the default is 50 I think?

## Considerations

Because the API looks like a static s3 file, there shouldn't be an issue with the request rate. however, please remain responsible with your requests if you choose to modify anything.

This program also does not persist any data, so you will see all available vaccine sites as "new listing" every time you run the program.
