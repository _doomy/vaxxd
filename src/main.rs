const URL: &'static str = "https://s3-us-west-2.amazonaws.com/mhc.cdn.content/vaccineAvailability.json";
use std::{collections::HashMap, fmt::Display, str::FromStr, thread, time::Duration};

use reqwest::{self, blocking::Client};
use serde::{Deserialize, Deserializer, de::{self, Unexpected}};
use serde_derive::{Serialize};
use clap::{App, Arg};
use geoutils::{Distance, Location};
use notify_rust::Notification;
fn main() {
    let matches = App::new("vaxxd").version("0.1")
        .arg(Arg::with_name("zip").short("c").long("config").takes_value(true).required(true).index(1))
        .arg(Arg::with_name("distance").short("d").long("distance").help("max distance from provided zip code to look in kilometers").takes_value(true).default_value("50"))
        .get_matches();
    let mut client = reqwest::blocking::Client::new();
    let search_coordinates = zip_codes::map().get(matches.value_of("zip").unwrap()).unwrap().coordinates.unwrap();
    let search_location = Location::new(search_coordinates.0, search_coordinates.1);
    let max_distance_meters: f64 = matches.value_of("distance").unwrap().parse::<f64>().unwrap() * 1000f64; // multiply for kilometers
    // keep this so we can notify of any changes
    let mut available_cache = HashMap::<usize, Site>::default();
    let mut events = Events::new();
    loop {
        match check(&mut client) {
            Ok(sites) => {               
                // compare available sites with our sites_cache to see if anything has changed
                for site in sites {
                    match available_cache.get(&site.id) {
                        Some(site) => {
                            // We already have this site, but we should remove it if it is market as unavailable
                            if !site.available { 
                                events.push(Event::RemoveFromCache{id: site.id, site: site.clone()});
                            }
                        }
                        None => {
                            // We don't have this site tracked yet
                            // Check to see if its available
                            if site.available { 
                                // check to see that it's within the location we want
                                let (lat, long): (f32, f32) = (site.lat.parse().unwrap(), site.long.parse().unwrap());
                                let location = Location::new(lat, long);
                                if location.distance_to(&search_location).unwrap().meters() <= max_distance_meters {
                                    // we have vaxxes!
                                    events.push(Event::AddToCache{id: site.id, site: site});
                                }
                            }
                        }
                    }
                }
            },
            Err(e) => println!("{}", e),
        }
        // process events
        for event in events.iter() {
            match event {
                Event::AddToCache { id, site } => {
                    println!("✅ New listing: {} - {}", site.address, site.url);
                    available_cache.insert(id, site.clone());
                    Notification::new()
                        .summary("💉 Hot vaxxes available in your area 💉")
                        .body(&format!("✅ New listing: {} - {}", site.address, site.url))
                        .show().unwrap();
                }
                Event::RemoveFromCache { id, site } => {
                    println!("❌ Removed listing: {} - {}", site.address, site.url);
                    available_cache.remove(&id);
                }
            }
        }
        let amount =  available_cache.len();

        // sleep for 10s
        thread::sleep(Duration::from_secs(10));
    }
}
#[derive(Default)]
pub struct Events(Vec<Event>);

impl Events {
    fn get(&self) -> &Vec<Event> {
        &self.0
    }
    fn get_mut(&mut self) -> &mut Vec<Event> {
        &mut self.0
    }
    pub fn push(&mut self, event: Event) {
        self.0.push(event);
    }
    pub fn new() -> Self {
        Self(vec![])
    }
    pub fn iter(&mut self) -> Vec<Event> {
        self.0.drain(..).collect::<Vec<Event>>()
    }
}

pub enum Event {
    RemoveFromCache {id: usize, site: Site},
    AddToCache {id: usize, site: Site},
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Site {
    #[serde(deserialize_with = "from_str")]
    id: usize,
    region: String,
    address: String,
    lat: String,
    long: String,
    #[serde(rename = "coach_url")]
    url: String,
    #[serde(rename = "availability")]
    #[serde(deserialize_with = "from_yes_no_str")]
    available: bool,
}

fn check(client: &mut Client) -> Result<Vec<Site>, reqwest::Error> {
    client.get(URL).send()?.json::<Vec<Site>>()
}

fn from_str<'de, T, D>(deserializer: D) -> Result<T, D::Error>
    where T: FromStr,
          T::Err: Display,
          D: Deserializer<'de>
{
    let s = String::deserialize(deserializer)?;
    T::from_str(&s).map_err(de::Error::custom)
}

fn from_yes_no_str<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    match String::deserialize(deserializer)?.as_str() {
        "no" => Ok(false),
        "yes" => Ok(true),
        other => Err(de::Error::invalid_value(
            Unexpected::Str(other),
            &"no or yes",
        )),
    }
}